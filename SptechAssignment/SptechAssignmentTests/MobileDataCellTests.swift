//
//  MobileDataCellTests.swift
//  SptechAssignmentTests
//
//  Created by ganga raju on 13/09/19.
//  Copyright © 2019 Sample. All rights reserved.
//
import XCTest
@testable import SptechAssignment

class MobileDataCellTests: XCTestCase {
    var testTable = UITableView()
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        testTable.register(MobileDataCell.self, forCellReuseIdentifier: "mobileDataCell")
        

    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testCustomCell() {
        let customCell: MobileDataCell = testTable.dequeueReusableCell(withIdentifier: "mobileDataCell") as! MobileDataCell
        XCTAssertNotNil(customCell, "No Custom Cell Available")
        
       // let recordYear1 = RecordYear(valueofData: "34.789", year: "2019",isdemonstrate: false)
        customCell.awakeFromNib()
       // customCell.bindData(record: recordYear1)
        customCell.setSelected(true, animated: true)
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
