//
//  RecordYearTests.swift
//  SptechAssignmentTests
//
//  Created by ganga raju on 14/09/19.
//  Copyright © 2019 Sample. All rights reserved.
//

import XCTest
@testable import SptechAssignment


class RecordYearTests: XCTestCase {
    var recordYear: RecordYear!
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        recordYear = RecordYear(valueofData: "19.5473643", year: "2018", isdemonstrate: false)

    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        recordYear = nil;
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    func testRecordYearSetter() {
        XCTAssertTrue(recordYear.year == "2018")
        XCTAssertTrue(recordYear.volumeOfMobileData == "19.5473643")
    }
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
