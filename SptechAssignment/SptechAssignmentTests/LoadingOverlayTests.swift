//
//  LoadingOverlayTests.swift
//  SptechAssignmentTests
//
//  Created by ganga raju on 15/09/19.
//  Copyright © 2019 Sample. All rights reserved.
//

import XCTest

class LoadingOverlayTests: XCTestCase {
    var loadingOverlay: LoadingOverlay!
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        loadingOverlay = LoadingOverlay.shared
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        loadingOverlay = nil;
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    func test_init_loadingOverlay(){
        
        /*Asserts that an expression is not nil.
         Generates a failure when expression == nil.*/
        XCTAssertNotNil( loadingOverlay )
    }
    func testShowloadingOvelay(){
        
        /*Asserts that an expression is not nil.
         Generates a failure when expression == nil.*/
       loadingOverlay.showOverlay(view: UIApplication.shared.keyWindow!)
    }
    func testHideOvelay(){
        
        /*Asserts that an expression is not nil.
         Generates a failure when expression == nil.*/
        loadingOverlay.hideOverlayView()
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
