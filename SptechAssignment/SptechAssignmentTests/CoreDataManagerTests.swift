//
//  CoreDataManagerTests.swift
//  SptechAssignmentTests
//
//  Created by ganga raju on 12/09/19.
//  Copyright © 2019 Sample. All rights reserved.
//

import XCTest
import CoreData
@testable import SptechAssignment

class CoreDataManagerTests: XCTestCase {

    var coreDataManager: CoreDataManager!
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        coreDataManager = CoreDataManager.sharedInstance
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    func test_init_coreDataManager(){
        
        let instance = coreDataManager
        /*Asserts that an expression is not nil.
         Generates a failure when expression == nil.*/
        XCTAssertNotNil( instance )
    }
    /*test if NSPersistentContainer(the actual core data stack) initializes successfully
     */
    func test_coreDataStackInitialization() {
        let coreDataPersistent = coreDataManager.persistentContainer
        /*Asserts that an expression is not nil.
         Generates a failure when expression == nil.*/
        
        let container = NSPersistentContainer(name: "MobileData12")
        container.loadPersistentStores(completionHandler: {_,_ in })
        
        XCTAssertNotNil( coreDataPersistent )
    }

    
    func testSaveInCoreDataWith(){
  
        
        let dataModel1 = MBRecord(volumeOfMobileData: "12.8909", quarter: "2018-Q1", dataId: 1)
   
        let dataModel2 = MBRecord(volumeOfMobileData: "15.8909", quarter: "2018-Q2", dataId: 1)
        
        let dataArray : [MBRecord] = [dataModel1,dataModel2]
        
        /*Asserts that an expression is not nil.
         Generates a failure when expression == nil.*/
        XCTAssertNotNil( dataModel1 )
        XCTAssertNotNil( dataModel2 )
        coreDataManager.saveInCoreDataWith(array: dataArray)
    }
   
    func testFecthData(){
    
        let dataModel1 = MBRecord(volumeOfMobileData: "12.5438", quarter: "2018-Q1", dataId: 1)
        
        let dataModel2 = MBRecord(volumeOfMobileData: "15.2890", quarter: "2018-Q2", dataId: 1)
         let dataModel3 = MBRecord(volumeOfMobileData: "14.6784", quarter: "2018-Q3", dataId: 1)
         let dataModel4 = MBRecord(volumeOfMobileData: "17.8912", quarter: "2018-Q4", dataId: 1)
        
        let dataArray : [MBRecord] = [dataModel1,dataModel2,dataModel3,dataModel4]
        coreDataManager.clearData()
        
        coreDataManager.saveInCoreDataWith(array: dataArray)
        let records = coreDataManager.fetchData()
        let record = records.first
        XCTAssertTrue(record?.year == "2018")
        XCTAssertTrue(record?.volumeOfMobileData == "60.4024")
        XCTAssertTrue(record?.isDemonstate == true)
        XCTAssertEqual(records.count, 1)

        
    }

    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
