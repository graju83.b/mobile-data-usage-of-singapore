//
//  MDTableViewControllerTests.swift
//  SptechAssignmentTests
//
//  Created by ganga raju on 13/09/19.
//  Copyright © 2019 Sample. All rights reserved.
//

import XCTest
import UIKit
@testable import SptechAssignment

class MDTableViewControllerTests: XCTestCase {
    var mdtableVc: MDTableViewController!
   
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        self.mdtableVc = (storyboard.instantiateViewController(withIdentifier: "mdTableViewController") as! MDTableViewController)
        self.mdtableVc.viewDidLoad()
        let barbutton = UIBarButtonItem(title: "Get Data", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
        self.mdtableVc.getDataBtnAction(barbutton);
        
        let imageButton:UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        self.mdtableVc.clcikImageAction(sender: imageButton)
       
    }
    func testRowHeight(){
        XCTAssertEqual(self.mdtableVc.tableView.rowHeight,86)
    }
    func testTableViewHasCells() {
        let cell = self.mdtableVc.tableView.dequeueReusableCell(withIdentifier: "mobileDataCell")
        
        XCTAssertNotNil(cell,
                        "TableView should be able to dequeue cell with identifier: 'Cell'")
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        self.mdtableVc  = nil
    }
    
    func testControllerHasTableView() {
        
        mdtableVc.loadViewIfNeeded()
        
        XCTAssertNotNil(mdtableVc.tableView,
                        "Controller should have a tableview")
    }
    func testFetchOfflineData(){
        self.mdtableVc.viewModel.fetchOfflineData(completion: {_ in })
    }
    func testFetchMobileData(){
        self.mdtableVc.viewModel.fetchMMobileData(completion: {_ in })
    }
    func testFetchOnlineData(){
        self.mdtableVc.viewModel.fetchOnlineData(completion: {_ in })
    }
    
    func testTableViewHasDataSource() {
        XCTAssertNotNil(self.mdtableVc.tableView.dataSource)
    }
  
    
    func testTableViewConformsToTableViewDataSourceProtocol() {
        XCTAssertTrue(self.mdtableVc.conforms(to: UITableViewDataSource.self))
        XCTAssertTrue(self.mdtableVc.responds(to: #selector(self.mdtableVc.numberOfSections(in:))))
        XCTAssertTrue(self.mdtableVc.responds(to: #selector(self.mdtableVc.tableView(_:numberOfRowsInSection:))))
        XCTAssertTrue(self.mdtableVc.responds(to: #selector(self.mdtableVc.tableView(_:cellForRowAt:))))
      
    
    }
   
    
    func testShowNodataAvailableAlert(){
         self.mdtableVc?.showNodataAvailableAlert();
    }
 

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
