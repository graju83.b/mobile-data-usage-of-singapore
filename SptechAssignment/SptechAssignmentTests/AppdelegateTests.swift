//
//  AppdelegateTests.swift
//  SptechAssignmentTests
//
//  Created by ganga raju on 15/09/19.
//  Copyright © 2019 Sample. All rights reserved.
//

import XCTest

class AppdelegateTests: XCTestCase {
    var appdelgegate = UIApplication.shared.delegate;
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
      
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testAppdelgateShared(){
        
        /*Asserts that an expression is not nil.
         Generates a failure when expression == nil.*/
        XCTAssertNotNil( appdelgegate )
    }
    func testAppdelegateMethods(){
        
    appdelgegate?.applicationWillTerminate?(UIApplication.shared)
     appdelgegate?.applicationDidEnterBackground?(UIApplication.shared)
    appdelgegate?.applicationWillResignActive?(UIApplication.shared)
   appdelgegate?.applicationWillEnterForeground?(UIApplication.shared)
    }
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
