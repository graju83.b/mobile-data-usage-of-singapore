//
//  CoreDataManager.swift
//  SptechAssignment
//
//  Created by ganga raju on 12/09/19.
//  Copyright © 2019 Sample. All rights reserved.
//

import UIKit
import CoreData
class CoreDataManager: NSObject {
    
    static let sharedInstance = CoreDataManager()
    private override init() {}
    
    // MARK: - Core Data stack
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "MobileData")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func clearData() {
        do {
            let context = CoreDataManager.sharedInstance.persistentContainer.viewContext
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: String(describing: Record.self))
            do {
                let objects  = try context.fetch(fetchRequest) as? [NSManagedObject]
                _ = objects.map{$0.map{context.delete($0)}}
                CoreDataManager.sharedInstance.saveContext()
            } catch let error {
                print("ERROR DELETING : \(error)")
            }
        }
    }
    
    func fetchData() -> [RecordYear]{
        let context = CoreDataManager.sharedInstance.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Record")
        do {
            let yearAndQuarterSort = NSSortDescriptor(key: "quarter", ascending: false)
            fetchRequest.sortDescriptors = [yearAndQuarterSort]
            
            fetchRequest.propertiesToGroupBy = ["year"]
            fetchRequest.propertiesToFetch = ["year"]
            fetchRequest.resultType = .dictionaryResultType
            let records  = try context.fetch(fetchRequest)
            let array = getMobileDatausge(yearsArray: records as! [Dictionary<String, String>])

            return array ;
        } catch {
            fatalError("Failed to fetch records: \(error)")
        }
    }
    
    func getMobileDatausge(yearsArray: [Dictionary<String, String>])  -> [RecordYear]{
        var mobileDataArray: [RecordYear] = []
        let context = CoreDataManager.sharedInstance.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<Record>(entityName: "Record")
        do {
            let yearAndQuarterSort = NSSortDescriptor(key: "quarter", ascending: true)
            fetchRequest.sortDescriptors = [yearAndQuarterSort]
        
            let records  = try context.fetch(fetchRequest)
            
            for dict in yearsArray {
                if let year = dict["year"] {
                    let resultArray = records.filter {
                        $0.year == year
                    }
                    let mobileData = getYearDataAndDemostrate(resultArray: resultArray, year: year)
                    let recordYear = RecordYear(valueofData: mobileData.yearDataUsage, year: year, isdemonstrate: mobileData.isAnydemonstate)
                    mobileDataArray.append(recordYear)
                }
            }
            return mobileDataArray;
        } catch {
            fatalError("Failed to fetch records: \(error)")
        }
        
    }
    
    func getYearDataAndDemostrate(resultArray:[Record],year: String) -> (yearDataUsage: String, isAnydemonstate: Bool){
        var sumOfValueData: Double? = 0.0;
        var isDemonstate: Bool = false;
        var quarterVlaue :Double = 0.0;
        
        for (index,yearData) in resultArray.enumerated() {
            let valumeOfData = Double((yearData as AnyObject).volumeOfMobileData!)
            if(index == 0){
                quarterVlaue = valumeOfData!;
            }else{
                if(quarterVlaue > valumeOfData!){
                    isDemonstate = true;
                }else{
                    quarterVlaue = valumeOfData!;
                }
                
            }
            sumOfValueData = sumOfValueData! + valumeOfData!
        }
        let stringValumeOfdata = String(sumOfValueData!)
        return (yearDataUsage:stringValumeOfdata,isAnydemonstate:isDemonstate)
    }
    
  
    
     func saveInCoreDataWith(array: [MBRecord]) {
        _ = array.map{self.createRecordsEntityFrom(record: $0)}
        do {
            try CoreDataManager.sharedInstance.persistentContainer.viewContext.save()
        } catch let error {
            print(error)
        }
    }
    
     func createRecordsEntityFrom(record: MBRecord) -> NSManagedObject? {
        
        let context = CoreDataManager.sharedInstance.persistentContainer.viewContext
        let recordsEntity = NSEntityDescription.insertNewObject(forEntityName: "Record", into: context)
        recordsEntity.setValue(record.volumeOfMobileData, forKey: "volumeOfMobileData")
        
        let sringsArray: [String] =  record.quarter.components(separatedBy: "-")
        if sringsArray.count>0{
            let year: String = sringsArray[0]
            recordsEntity.setValue(year, forKey: "year");
        }
        recordsEntity.setValue(record.quarter, forKey: "quarter");
        recordsEntity.setValue(record.dataId, forKey: "dataId");
        
        return recordsEntity
        
    }
}


