//
//  RecordYear.swift
//  SptechAssignment
//
//  Created by ganga raju on 12/09/19.
//  Copyright © 2019 Sample. All rights reserved.
//
import Foundation

struct RecordYear {
    let volumeOfMobileData:String
    let year:String
    let isDemonstate: Bool
    init(valueofData: String, year: String, isdemonstrate : Bool = false){
        self.volumeOfMobileData = valueofData
        self.year = year
        self.isDemonstate = isdemonstrate
    }
}
