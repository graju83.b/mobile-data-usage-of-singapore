//
//  MobileData.swift
//  SptechAssignment
//
//  Created by ganga raju on 11/09/19.
//  Copyright © 2019 Sample. All rights reserved.
//
import CoreData
import Foundation


struct MBRecord: Codable {
    let volumeOfMobileData:String
    let quarter:String
    let dataId:Int
 
    private enum CodingKeys: String, CodingKey {
        case volumeOfMobileData = "volume_of_mobile_data"
        case quarter
        case dataId = "_id"
    }
}
struct Result: Codable{
    let records: [MBRecord]
    let resource_id:String
    let limit:Int
    let total:Int
    
}
struct MobileData: Codable {
    let result: Result
    
}
