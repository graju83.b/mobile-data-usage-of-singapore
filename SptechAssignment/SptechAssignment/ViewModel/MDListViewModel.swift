//
//  MDListViewModel.swift
//  SptechAssignment
//
//  Created by ganga raju on 13/09/19.
//  Copyright © 2019 Sample. All rights reserved.
//

import UIKit
import CoreData


struct MobileDatarequest: RequestType{
    typealias ResponseType = MobileData
   var data: RequestData {
        return RequestData(method: .get, path: "/datastore_search?resource_id=a807b7ab-6cad-4aa6-87d0-e283a7353a0f&limit=59")
    }
   
}

class MDListViewModel: NSObject {
    
 
    
    public var mobileDataArray: [RecordYear] = []
    typealias FetchMobileDataCompletion = ((_ isSuccess: Bool) -> Void)
    
    func fetchMMobileData(completion: @escaping FetchMobileDataCompletion){
       fetchOnlineData(completion: completion);
    }
    
    func fetchOfflineData(completion: @escaping FetchMobileDataCompletion){
        self.mobileDataArray = CoreDataManager.sharedInstance.fetchData()
        completion(true)
    }
    
    func fetchOnlineData(completion: @escaping FetchMobileDataCompletion){
        MobileDatarequest().serviceCall(
            onSuccess: { (mobileResponseData: MobileData) in
                CoreDataManager.sharedInstance.clearData()
                CoreDataManager.sharedInstance.saveInCoreDataWith(array: mobileResponseData.result.records)
                self.mobileDataArray = CoreDataManager.sharedInstance.fetchData()
                completion(true)
        },
            onError: { (error: Error) in
                switch error.localizedDescription {
                    case ErrorMessages.noConnection.rawValue: self.fetchOfflineData(completion: completion)
                default:
                    print(error.localizedDescription)
                }
              
            
        })
    }
  
    
}
