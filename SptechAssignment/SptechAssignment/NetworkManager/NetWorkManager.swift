//
//  NetWorkManager.swift
//  FXHelper
//
//  Created by ganga raju on 07/09/19.
//  Copyright © 2019 Sample. All rights reserved.
//




import UIKit


public enum HTTPMethod: String{
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case delete = "DELETE"
    
    
}
enum NetworkEnvironment{
    case qa
    case production
    case staging
}
public enum ConnError: Swift.Error {
    case invalidURL
    case noData
    case notReachble
}
public enum ErrorMessages: String {
    case noConnection = "The Internet connection appears to be offline."
}

public struct RequestData{
    let url:String;
    let httpMethod:HTTPMethod;
    let bodyParms:[String: Any?]?
    let headers:[String:String]?

    
    
    init( method: HTTPMethod = .get,
          path: String,
          bodyParmaneters: [String: Any?]? = nil,
          headers: [String:String]? = nil) {
        
        self.httpMethod = method
        self.bodyParms = bodyParmaneters
        self.headers = headers
        
        
        let environment: NetworkEnvironment = .qa;
        var baseUrl : String{
            switch  environment{
            case .production: return "https://data.gov.sg/api/action/"
            case .qa: return "https://data.gov.sg/api/action/"
            case .staging: return "https://data.gov.sg/api/action/"
            }
        }
        self.url = baseUrl+path
    }
    
    
    
}
public protocol RequestType{
    associatedtype ResponseType: Codable
    var data: RequestData { get }
}
public extension RequestType {
    func serviceCall( onSuccess: @escaping (ResponseType) -> Void,
                      onError:@escaping (Error) -> Void) {
        let dispatcher: NetworkDispatcher = URLSessionNetworkDispatcher.instance
        dispatcher.dispatch(request: self.data,  onSuccess: { (responseData: Data) in
            do {
                let jsonDecoder = JSONDecoder()
                let result = try jsonDecoder.decode(ResponseType.self, from: responseData)
                DispatchQueue.main.async {
                    onSuccess(result)
                    
                }
            } catch let error {
                DispatchQueue.main.async {
                    print("Decode  Error")
                    onError(error)
                }
            }
        },
        onError: { (error: Error) in
                DispatchQueue.main.async {
                    onError(error)
                }
            }
        )
        
    }
}

public protocol NetworkDispatcher {
    func dispatch(request: RequestData, onSuccess: @escaping (Data) -> Void, onError: @escaping (Error) -> Void)
}

public struct URLSessionNetworkDispatcher: NetworkDispatcher {
    public static let instance = URLSessionNetworkDispatcher()
    private init() {}
    
    public func dispatch(request: RequestData, onSuccess: @escaping (Data) -> Void, onError: @escaping (Error) -> Void) {
    
        guard let url = URL(string: request.url) else {
            onError(ConnError.invalidURL)
            return
        }
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = request.httpMethod.rawValue
        
        do {
            if let params = request.bodyParms {
                urlRequest.httpBody = try JSONSerialization.data(withJSONObject: params, options: [])
            }
        } catch let error {
            onError(error)
            return
        }
        
        if let headers = request.headers {
            urlRequest.allHTTPHeaderFields = headers
        }
       
        URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            if let error = error {
              
               onError(error)
                
                return
            }
           
            
            
            guard let _data = data else {
                onError(ConnError.noData)
                return
            }
            let str = String(decoding: data!, as: UTF8.self)
            print(str)

            onSuccess(_data)
            }.resume()
    }
}
