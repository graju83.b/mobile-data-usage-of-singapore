//
//  MobileDataCell.swift
//  SptechAssignment
//
//  Created by ganga raju on 11/09/19.
//  Copyright © 2019 Sample. All rights reserved.
//

import UIKit

class MobileDataCell: UITableViewCell {
    

    let containerView:UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.clipsToBounds = true // this will make sure its children do not go out of the boundary
        return view
    }()
    
    let clickbleImage:UIButton = {
        let img = UIButton()
        img.contentMode = .scaleAspectFill // image will never be strecthed vertially or horizontally
        img.translatesAutoresizingMaskIntoConstraints = false // enable autolayout
        img.layer.cornerRadius = 6
        img.clipsToBounds = true
        return img
    }()
    
    let yearLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.textColor = .black
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let dataUsageLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textColor = .black
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = .clear
        containerView.addSubview(yearLabel)
        containerView.addSubview(dataUsageLabel)
        containerView.addSubview(clickbleImage)
        self.contentView.addSubview(containerView)
        
        
        // set the shadow of the view's layer
        containerView.layer.backgroundColor = UIColor.clear.cgColor
        containerView.layer.shadowColor = UIColor.black.cgColor
        containerView.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        containerView.layer.shadowOpacity = 0.2
        containerView.layer.shadowRadius = 4.0
        
        // set the cornerRadius of the containerView's layer
        containerView.layer.cornerRadius = 6
        containerView.layer.masksToBounds = true
    containerView.centerYAnchor.constraint(equalTo:self.contentView.centerYAnchor).isActive = true
         containerView.leadingAnchor.constraint(equalTo:self.contentView.leadingAnchor, constant:10).isActive = true
        containerView.trailingAnchor.constraint(equalTo:self.contentView.trailingAnchor, constant:-10).isActive = true
        containerView.heightAnchor.constraint(equalToConstant:70).isActive = true
        
        containerView.backgroundColor = .white
        containerView.isUserInteractionEnabled = true
        
        yearLabel.topAnchor.constraint(equalTo:self.containerView.topAnchor).isActive = true
    yearLabel.leadingAnchor.constraint(equalTo:self.containerView.leadingAnchor,constant: 10).isActive = true
    yearLabel.trailingAnchor.constraint(equalTo:self.containerView.trailingAnchor,constant:-68).isActive = true
        yearLabel.heightAnchor.constraint(equalToConstant:35).isActive = true
        
    
    
      dataUsageLabel.topAnchor.constraint(equalTo:self.yearLabel.bottomAnchor).isActive = true
    dataUsageLabel.leadingAnchor.constraint(equalTo:self.containerView.leadingAnchor,constant: 10).isActive = true
       dataUsageLabel.topAnchor.constraint(equalTo:self.yearLabel.bottomAnchor).isActive = true
       
        containerView.heightAnchor.constraint(equalToConstant:70).isActive = true
    dataUsageLabel.trailingAnchor.constraint(equalTo:self.containerView.trailingAnchor,constant:-68).isActive = true
        dataUsageLabel.heightAnchor.constraint(equalToConstant:35).isActive = true
        
    clickbleImage.centerYAnchor.constraint(equalTo:self.contentView.centerYAnchor).isActive = true
    clickbleImage.leadingAnchor.constraint(equalTo:self.dataUsageLabel.trailingAnchor, constant:10).isActive = true
        clickbleImage.widthAnchor.constraint(equalToConstant:40).isActive = true
        clickbleImage.heightAnchor.constraint(equalToConstant:40).isActive = true
        clickbleImage.backgroundColor = .red
        
        //clickbleImage.addTarget(self, action: #selector(MobileDataCell.clcikImageAction), for: UIControl.Event.touchUpInside)
        clickbleImage.setImage(UIImage(imageLiteralResourceName: "sampleBar.png"), for: UIControl.State.normal)
      


    }
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
    }
    func bindData(record: RecordYear) {
        self.yearLabel.text = record.year;
        self.dataUsageLabel.text = record.volumeOfMobileData;
        self.clickbleImage.isHidden = !record.isDemonstate
    }
   
    
}
