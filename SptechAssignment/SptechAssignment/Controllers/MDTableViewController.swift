//
//  MDTableViewController.swift
//  SptechAssignment
//
//  Created by ganga raju on 13/09/19.
//  Copyright © 2019 Sample. All rights reserved.
//

import UIKit

class MDTableViewController: UITableViewController {
    private let cellReuseIdentifier: String = "mobileDataCell"
    var viewModel: MDListViewModel!
    
    func showNodataAvailableAlert(){
        let alert = UIAlertController(title: "", message: "No Data Available", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    @objc func clcikImageAction(sender:UIButton) {
        print("Image clicked")
        
        let alert = UIAlertController(title: "", message: "Image Clicked", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    
    @IBAction func getDataBtnAction(_ sender: UIBarButtonItem) {
        // fetch mobile value data
        viewModel.fetchMMobileData { (isSuccess) in
            //show loading Indicator
            LoadingOverlay.shared.showOverlay(view: UIApplication.shared.keyWindow!)
            
            if isSuccess {
                //remove loading indcator
                LoadingOverlay.shared.hideOverlayView()
                //checking data is there or not
                if(self.viewModel.mobileDataArray.count == 0){
                    self.showNodataAvailableAlert();
                }else{
                    // reload data
                    self.tableView.reloadData()
                }
                
              
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Mobile Data Usage"
        self.tableView.rowHeight = 86;
        // setup view model
        viewModel = MDListViewModel()
        self.tableView.register(MobileDataCell.self, forCellReuseIdentifier: cellReuseIdentifier)
        self.tableView.backgroundColor = .groupTableViewBackground
        
    }
    
}

extension MDTableViewController{
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return viewModel.mobileDataArray.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as? MobileDataCell {
            
            let recordYear = viewModel.mobileDataArray[indexPath.row]
            cell.bindData(record: recordYear)
            cell.clickbleImage.addTarget(self, action: #selector(MDTableViewController.clcikImageAction), for: UIControl.Event.touchUpInside)
            
            return cell
            
        }
        return UITableViewCell()
        
    
     
    }
    
    
    
}
