### Mobile data usage of Singapore
### Author: Gangaraju Beduduru

In this Application we can see the mobile data usage of Singapore for yearly and display a clickable image in the entry if any quarter in a year demonstrates a decrease in volume data. 

This application support for online and offline data

Once open the application we can see  'Get Data'  button on navigation bar, once click on the button get the data from server and saved into the core data and displaying the list

### Requirements
For this to work, you need to have\

- Xcode 10.2  and Swift 4.2 installed on your machine\

